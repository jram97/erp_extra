const Pool = require('pg').Pool
const multer = require('multer');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser')
const fs = require('fs');
require('dotenv').config({ path: '.env' })

const readXlsxFile = require('read-excel-file/node');

const app = express();
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

global.__basedir = __dirname;

// -> Multer Upload Storage
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __basedir + '/uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
});

const upload = multer({ storage: storage });

// -> Express Upload Users
app.post('/api/upload-user', upload.single("users"), (req, res) => {
    importExcelUserData2PostgreSQL(__basedir + '/uploads/' + req.file.filename);
    res.json({
        'msg': 'File uploaded/import successfully!', 'file': req.file
    });
});

//adding testing endpoint
app.get('/api/select', async (req, res) => {
    
    const pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_DB,
        password: process.env.DB_PASS,
        port: parseInt(process.env.DB_PORT),
        sslmode: process.env.DB_SSL,
        ssl: {
            rejectUnauthorized: false,
            ca: fs.readFileSync('ca-certificate.crt').toString(),
          }
    });

    console.log('Request received...')
    const result = await pool.query(`SELECT * FROM products`);
    res.send(result)
    console.log(result);
  })

// -> Express Upload Products
app.post('/api/upload-product', upload.single("products"), (req, res) => {
    importExcelProductData2PostgreSQL(__basedir + '/uploads/' + req.file.filename);
    res.json({
        'msg': 'File uploaded/import successfully!', 'file': req.file
    });
});

// -> Express Update Status
app.post('/api/update-status', (req, res) => {
    const { id, status } = req.body;
    updateStatus2PostgreSQL(status, id);
    res.json({
        'msg': `Product modified with ID: ${id}`
    });
});

// -> Express Update Closed
app.post('/api/update-closed', (req, res) => {
    const { id, status } = req.body;
    updateClosed2PostgreSQL(status, id);
    res.json({
        'msg': `Venta modified with ID: ${id}`
    });
});

// -> Express Update Stock
app.post('/api/update-stock', (req, res) => {
    const { id, quantity } = req.body;
    updateStock2PostgreSQL(quantity, parseInt(id));
    res.json({
        'msg': `Product modified with ID: ${id}`
    });
});

// -> Express Update Liquidación
app.post('/api/update-liq', (req, res) => {
    const { id, status } = req.body;
    updateLiquidacion2PostgreSQL(status, id);
    res.json({
        'msg': `Product modified with ID: ${id}`
    });
});

// -> Import User Excel Data to PostgreSQL database
const importExcelUserData2PostgreSQL = (filePath) => {
    // Create a connection to the database
    const pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_DB,
        password: process.env.DB_PASS,
        port: parseInt(process.env.DB_PORT),
        sslmode: process.env.DB_SSL,
        ssl: {
            rejectUnauthorized: false,
            ca: fs.readFileSync('ca-certificate.crt').toString(),
          }
    });

    // File path.
    readXlsxFile(filePath).then((rows) => {
        // Remove Header ROW
        rows.shift();
        let data = [];
        // `rows` is an array of rows
        // each row being an array of cells.	 
        for (let index = 0; index < rows.length; index++) {
            data.push({ ...rows[index] })
        }
        data.map((info, index) => {
            // Open the PostgreSQL connection and save info
            pool.query('INSERT INTO clients ("codClient", "name", "created_by", "updated_by", "ruta", "clasificacion", "agencia", "giro", "estado", "departamento", "municipio", "fecha_apertura", "id_ruta", "referencia", "calle_avenida", "telpropietario", "semanas", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo", "propietario") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25)', [info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9], info[10], info[11], info[12], info[13], info[14], info[15], info[16], info[17], info[18], info[19], info[20], info[21], info[22], info[23], info[24]], (error, results) => {
                if (error) {
                    throw error
                }
                console.log(`User ${info[0]}, added`)
            })
        })
    })
}
// -> Import Product Excel Data to PostgreSQL database
const importExcelProductData2PostgreSQL = (filePath) => {
    // Create a connection to the database
    const pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_DB,
        password: process.env.DB_PASS,
        port: parseInt(process.env.DB_PORT),
        sslmode: process.env.DB_SSL,
        ssl: {
            rejectUnauthorized: false,
            ca: fs.readFileSync('ca-certificate.crt').toString(),
          }
    });

    // File path.
    readXlsxFile(filePath).then((rows) => {
        // Remove Header ROW
        rows.shift();
        let data = [];
        // `rows` is an array of rows
        // each row being an array of cells.	 
        for (let index = 0; index < rows.length; index++) {
            data.push({ ...rows[index] })
        }
        data.map((info, index) => {
            // Open the PostgreSQL connection and save info
            pool.query('INSERT INTO products ("name", "stock", "price", "status", "isAssigned") VALUES ($1, $2, $3, $4, $5)', [info[0], info[1], info[2], info[3], info[4]], (error, results) => {
                if (error) {
                    throw error
                }
                console.log(`Product ${info[0]}, added`)
            })
        })
    })
}
// -> Update Liquidación from ERP to PostgreSQL database
const updateLiquidacion2PostgreSQL = (status, ids) => {
    // Create a connection to the database
    const pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_DB,
        password: process.env.DB_PASS,
        port: parseInt(process.env.DB_PORT),
        sslmode: process.env.DB_SSL,
        ssl: {
            rejectUnauthorized: false,
            ca: fs.readFileSync('ca-certificate.crt').toString(),
          }
    });

    ids.map((id) => {
        // Open the PostgreSQL connection and save info
        pool.query('UPDATE ventas SET "isliquidado" = $1 WHERE id = $2', [status, id], (error, results) => {
            if (error) {
                throw error
            }
            console.log('UPDATE ventas SET "isliquidado" = ' + status + ' WHERE "id" = ' + id)
        })
    })
    pool.end()
}
// -> Update Status from ERP to PostgreSQL database
const updateClosed2PostgreSQL = (status, ids) => {
    // Create a connection to the database
    const pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_DB,
        password: process.env.DB_PASS,
        port: parseInt(process.env.DB_PORT),
        sslmode: process.env.DB_SSL,
        ssl: {
            rejectUnauthorized: false,
            ca: fs.readFileSync('ca-certificate.crt').toString(),
          }
    });

    ids.map((id) => {
        // Open the PostgreSQL connection and save info
        pool.query('UPDATE ventas SET "isclosed" = $1 WHERE id = $2', [status, id], (error, results) => {
            if (error) {
                throw error
            }
            console.log('UPDATE ventas SET "isclosed" = ' + status + ' WHERE "id" = ' + id)
        })
    })
    pool.end()
}
// -> Update Status from ERP to PostgreSQL database
const updateStatus2PostgreSQL = (status, ids) => {
    // Create a connection to the database
    const pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_DB,
        password: process.env.DB_PASS,
        port: parseInt(process.env.DB_PORT),
        sslmode: process.env.DB_SSL,
        ssl: {
            rejectUnauthorized: false,
            ca: fs.readFileSync('ca-certificate.crt').toString(),
          }
    });

    ids.map((id) => {
        // Open the PostgreSQL connection and save info
        pool.query('UPDATE products SET "isAssigned" = $1 WHERE "id" = $2', [status, id], (error, results) => {
            if (error) {
                throw error
            }
            console.log('UPDATE products SET "isAssigned" = ' + status + ' WHERE "id" = ' + id)
        })
    })

    pool.end()

}
// -> Update Stock from ERP to PostgreSQL database
const updateStock2PostgreSQL = (quantity, id) => {

    // Create a connection to the database
    const pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_DB,
        password: process.env.DB_PASS,
        port: parseInt(process.env.DB_PORT),
        sslmode: process.env.DB_SSL,
        ssl: {
            rejectUnauthorized: false,
            ca: fs.readFileSync('ca-certificate.crt').toString(),
          }
    });

    let result;
    // Open the PostgreSQL connection and get info
    pool.query('SELECT * FROM products WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        } else {
            // Get stock from one product
            const { stock } = results.rows[0];
            if (stock && stock > 0) {
                result = (parseInt(stock) - parseInt(quantity))
                // Open the PostgreSQL connection and update info
                pool.query('UPDATE products SET "stock" = $1 WHERE id = $2', [result, id], (error, results) => {
                    if (error) {
                        throw error
                    }
                    console.log(`Product modified with ID: ${id}`)
                })
            } else {
                console.log(`Product not modified, ID: ${id}`)
            }
        }
    })
}

// Create a Server
let server = app.listen(8080, function () {

    let host = server.address().address
    let port = server.address().port

    console.log("App listening at http://%s:%s", host, port)
})
